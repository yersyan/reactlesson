import React, {useEffect, useState} from "react"
import Logo from "../logo/logo";
import Menu from "../menu/menu";
import css from "./header.module.scss"
import Button from "../ui/button";

const Header = ({setToken}) => {


    const logout = () => {
        localStorage.removeItem('token')
        setToken(null)
    }


    return (
        <header className={css.header}>
           <Logo/>
           <Menu/>
            <Button title="Logout" color="blue" onClick={logout}>Logout</Button>
        </header>
    )
}

export default Header