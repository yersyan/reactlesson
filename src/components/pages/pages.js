import React from 'react';
import {Route, Routes, Navigate} from "react-router-dom";
import {authRoutes, routes} from "./routes";
import {AUTH_PAGE, HOME_PAGE} from "../../utils/urls";

const Pages = () => {

    const isAuth = localStorage.getItem('token')

    return (
        <Routes>
            {/*деструктуризация*/}
            {
                isAuth
                    ? authRoutes.map(({path, element}) => {
                        return <Route
                            key={path}
                            path={path}
                            element={element}
                            // hasceneri chisht fixum
                            exact='true'
                        />
                    })
                    :  routes.map(({path, element}) => {
                        return <Route
                            key={path}
                            path={path}
                            element={element}
                            exact='true'
                        />
                    })
            }
            <Route
                path={'*'}
                element={<Navigate to={isAuth ? HOME_PAGE : AUTH_PAGE}/>}
                exact="true"
            />
        </Routes>
    );
};

export default Pages;