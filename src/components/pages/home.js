import React, {useEffect, useState} from 'react';

const Home = () => {

    const [count, setCount] = useState(0)
    const [text, setText] = useState('Armenia')

    const up = () => {
        setCount(count + 1)
    }

    const down = () => {
        setCount(count - 1)
    }

    const changeText = () => {
        setText('Yerevan')
    }

    useEffect(() => {
        console.log(text)
    }, [count])



    return (
        <div>
            <div>{count}</div>
            <button onClick={up}>Up</button>
            <button onClick={down}>Down</button>
            <div>{text}</div>
            <button onClick={changeText}>change text</button>
        </div>
    );
};

export default Home;