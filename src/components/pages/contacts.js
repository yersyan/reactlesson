import React, {useEffect, useState} from 'react';
import css from './pages.module.scss'

const Contacts = () => {
 const [number,setNumber] = useState(0)
const [bg,setBg] = useState("transparent")
useEffect(()=>{
   switch (number){
       case 5 :
           setBg("red")
           break
       case 10 :
           setBg("green")
           break
       case 15 :
           setBg("blue")
           break
       case 20 :
           setBg("yellow")
           break
       default: setBg("transparent")
   }
},[number])
    return (
        <div>
            <div className={css.block} style={{background: bg}} >
                {number}
            </div>
            <button onClick={()=>setNumber(number + 5 )} >up</button>
            <button onClick={()=>setNumber(number - 5 )} >down</button>

        </div>
    );
};

export default Contacts;