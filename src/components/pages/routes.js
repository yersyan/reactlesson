import {ABOUT_PAGE, AUTH_PAGE, CONTACTS_PAGE, HOME_PAGE, PORTFOLIO_PAGE, SKILLS_PAGE} from "../../utils/urls";
import Home from "./home";
import Auth from "./auth";
import Skills from "./skills";
import Portfolio from "./portfolio";
import About from "./about";
import Contacts from "./contacts";

export const routes = [
    {
        name: 'Auth',
        path: AUTH_PAGE,
        element: <Auth/>
    }
]

export const authRoutes = [
    {
        name: 'Home',
        path: HOME_PAGE,
        element: <Home/>
    },
    {
        name: 'Contacts',
        path: CONTACTS_PAGE,
        element: <Contacts/>
    },
    {
        name: 'About',
        path: ABOUT_PAGE,
        element: <About/>
    },
    {
        name: 'Portfolio',
        path: PORTFOLIO_PAGE,
        element: <Portfolio/>
    },
    {
        name: 'Skills',
        path: SKILLS_PAGE,
        element: <Skills/>
    },
]