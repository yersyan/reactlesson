import React from 'react';
import Form from "../ui/form";
import css from "./pages.module.scss"

const Auth = () => {
    return (
        <div className={css.auth}>
            <Form/>
        </div>
    );
};

export default Auth;