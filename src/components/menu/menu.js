import React from 'react';
import css from './menu.module.scss'
import {authRoutes, routes} from "../pages/routes";
import {Link} from "react-router-dom";
import {useLocation} from "react-router-dom";

const Menu = () => {

    const {pathname} = useLocation()

    const isAuth = localStorage.getItem('token')

    return (
        <ul className={css.menu}>
            {
                isAuth
                    ? authRoutes.map(({path, name}) => {
                        return <li key={path}>
                            <Link
                                className={pathname === path ? css.active : undefined}
                                to={path}
                                exact='true'
                            >
                                {name}
                            </Link>
                        </li>
                    })
                    : routes.map(({path, name}) => {
                        return <li key={path}>
                            <Link
                                className={pathname === path ? css.active : undefined}
                                to={path}
                                exact='true'
                            >
                                {name}
                            </Link>
                        </li>
                    })
            }
        </ul>
    );
};

export default Menu;