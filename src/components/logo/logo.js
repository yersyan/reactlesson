import React from 'react';
import logo from "../../images/logo.png";
import css from"./logo.module.scss"

const Logo = () => {
    return (
        <div className={css.logo}>
            <img src={logo} alt="logo"/>
        </div>
    );
};

export default Logo;