import React from 'react';
import css from './main.module.scss'
import Pages from "../pages/pages";

const Main = () => {

    return (
        <main className={css.main}>
            <Pages/>
        </main>
    );
};

export default Main;