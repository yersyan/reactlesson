import React from 'react';
import css from './ui.module.scss'

const Input = ({title, ...props}) => {

    return (
        <label className={css.label}>
            <span>{title}</span>
            <input
                {...props}
            />
        </label>
    );
};

export default Input;