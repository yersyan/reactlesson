import React, {useState} from 'react';
import css from './ui.module.scss'
import Button from "./button";
import Input from "./input";
import {Link, useNavigate} from "react-router-dom"
import {useQuery} from "../../hooks/useQuery";
import {AUTH_PAGE} from "../../utils/urls";

const Form = () => {

    const navigate = useNavigate()

    const formQuery = useQuery().get('form')
    const loginForm = formQuery === 'login'




    const [username, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')


    const onChangeUsername = (e) => {
        setUsername(e.target.value)
    }
    const onChangeEmail = (e) => {
        setEmail(e.target.value)
    }
    const onChangePassword = (e) => {
        setPassword(e.target.value)
    }

    const register = (e) => {
        e.preventDefault()
        localStorage.setItem('user', JSON.stringify({
            username,
            email,
            password
        }))
        navigate('/auth?form=login')
    }

    const login = (e) => {
        e.preventDefault()
        let localEmail = JSON.parse(localStorage.getItem('user'))['email']
        let localPassword = JSON.parse(localStorage.getItem('user'))['password']

        if(localEmail === email && localPassword === password){
            localStorage.setItem('token', 'hkjhjkhkjhjkha74541fdasfdas')
            window.location.reload()
        }
    }

    return (
        <form className={css.form} onSubmit={loginForm ? login : register}>
            {
                !loginForm
                && <Input
                title="Username"
                type="text"
                placeholder="Username"
                value={username}
                onChange={onChangeUsername}
            />}
            <Input
                title="Email"
                type="email"
                placeholder="Email"
                value={email}
                onChange={onChangeEmail}
            />
            <Input
                title="Password"
                type="password"
                placeholder="Password"
                value={password}
                onChange={onChangePassword}
            />
            <Button>
                {loginForm ? "Login" : "Register"}
            </Button>
            {
                loginForm
                    ? <span>For register <Link to={AUTH_PAGE}>sign up</Link></span>
                    : <span>For login <Link to={`${AUTH_PAGE}?form=login`}>sign in</Link></span>
            }

            {/*<span>*/}
            {/*   For {loginForm ? "register" : "login"} <Link to={loginForm ? AUTH_PAGE : `${AUTH_PAGE}?form=login`}>*/}
            {/*    {loginForm ? "sign up" : "sign in"}*/}
            {/*   </Link>*/}
            {/*</span>*/}
        </form>
    );
};

export default Form;