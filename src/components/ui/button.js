import React from 'react';
import css from'./ui.module.scss'




const Button = ({children, ...props}) => {

    return (
        <button
            className={css.btn}
            {...props}
        >
            {children}
        </button>
    );
};

export default Button;