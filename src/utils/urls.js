export const HOME_PAGE = '/'
export const CONTACTS_PAGE = '/contacts'
export const SKILLS_PAGE = '/skills'
export const ABOUT_PAGE = '/about'
export const PORTFOLIO_PAGE = '/portfolio'
export const AUTH_PAGE = '/auth'

