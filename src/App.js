import React, {useEffect, useState} from "react";
import Header from "./components/header/header";
import Main from "./components/main/main";
import Footer from "./components/footer/footer";
import {AUTH_PAGE} from "./utils/urls";
import {useNavigate} from "react-router-dom";

function App() {

    const navigate = useNavigate()

    const [token, setToken] = useState(null)

    useEffect(() => {
        if(localStorage.getItem('token')){
            setToken(localStorage.getItem('token'))
        } else {
            navigate(AUTH_PAGE)
        }
    }, [token])


  return (
      <div className="container">
        <Header setToken={setToken}/>
        <Main/>
        <Footer/>
      </div>
  );
}

export default App;
